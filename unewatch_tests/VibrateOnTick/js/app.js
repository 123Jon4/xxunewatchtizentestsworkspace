var flashTimeout;

function main() {
	document.addEventListener('rotarydetent', function(ev) {
		onBezel(ev.detail.direction);
	});
}

function onBezel(direction) {
	navigator.vibrate(0);
	// navigator.vibrate([1000, 1000, 2000, 2000, 1000]);
	navigator.vibrate([ 50 ]);

	document.body.style.background = "white";

	if (flashTimeout)
		clearTimeout(flashTimeout);
	flashTimeout = setTimeout(function() {
		document.body.style.background = "";
	}, 50);
}

main();