// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import App from './App';
import router from './router';

const VueTouch = require('vue-touch');

require('es6-promise').polyfill(); // Tizen doesn't natively support Promises, TODO: See if Babel polyfill makes this obsolete

Vue.config.productionTip = false;

Vue.use(VueTouch, { name: 'v-touch' });

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
});
