import Vue from 'vue';
import Router from 'vue-router';
//import Backend3000Routes from '../test_cases/Backend3000/_specific/routes';
//import SmoothInteractionRoutes from '../test_cases/SmoothInteraction/_specific/routes';
//import DemoAppRoutes from '../test_cases/DemoApp_Mk_I/_specific/routes';
//import Bezel from '../test_cases/Bezel/_specific/routes';
import StandbyToArtefact from '../test_cases/DemoApp_Mk_II/_specific/routes';

Vue.use(Router);

export default new Router({
  routes: [StandbyToArtefact]
});
