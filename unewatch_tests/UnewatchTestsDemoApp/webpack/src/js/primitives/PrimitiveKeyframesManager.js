import EventEmitter from 'wolfy87-eventemitter';
import TypeSafety from '../../test_cases/Backend3000/js/helpers/TypeSafety';
import StoryNodeKeyframes from '../../test_cases/Backend3000/js/story_driven_logic/story/v1_0/StoryNodeKeyframes';

export default class PrimitiveKeyframesManager {
  static EVENTS = {
    ON_KEYFRAME: Symbol('onKeyframe').toString(), // TODO: Figure out why the EventEmitter doesn't work with Symbols
    ON_SPECIAL_KEYFRAME: Symbol('onSpecialKeyframe').toString()
  };
  _storyNodeKeyframes = [];
  _specialKeyframes = {};
  _timedKeyframes = {};
  _keyframesQueue = [];
  _lastTime = 0;
  _languageCode; // TODO: Set this from a static default language for all Language-based classes

  _ee = new EventEmitter();

  constructor(storyNodeKeyframes, languageCode) {
    if (storyNodeKeyframes && languageCode) {
      this.init(storyNodeKeyframes, languageCode);
    }
  }

  init(storyNodeKeyframes, languageCode) {
    this.loadStoryNodeKeyframes(storyNodeKeyframes);
    this.setLanguage(languageCode);
  }

  setLanguage(code) {
    this._languageCode = code;
    this._mergeKeyframes();
  }

  loadStoryNodeKeyframes(storyNodeKeyframes) {
    TypeSafety.brutalTypeChecking(storyNodeKeyframes, StoryNodeKeyframes);

    this._storyNodeKeyframes = storyNodeKeyframes;
    this._mergeKeyframes();
  }

  _getLocaleSpecificKeyframes() {
    const localeKeyframes = this._storyNodeKeyframes.localeSpecific[this._languageCode];
    return localeKeyframes || [];
  }

  _hasGeneralKeyframeWithSameTimecode(keyframe) {
    return !!this._getGeneralKeyframeWithSameTimecode(keyframe);
  }

  _getGeneralKeyframeWithSameTimecode(keyframe) {
    const keyframes = this._storyNodeKeyframes.general.filter(
      gKey => gKey.at === keyframe.at);
    return keyframes.length > 0 ? keyframes[0] : null;
  }

  hasSpecialKeyframe(atSpecial) {
    return !!this.getSpecialKeyframe(atSpecial);
  }

  getSpecialKeyframe(atSpecial) {
    return this._fireSpecialKeyframe(atSpecial);
  }

  setTime(time) {
    this._lastTime = time;
    return this._fireKeyframes(time);
  }

  hasKeyframesInQueue() {
    return this._keyframesQueue.length > 0;
  }

  _fireSpecialKeyframe(atSpecial) {
    const keyframes = this._specialKeyframes.filter(
      k => k.atSpecial === atSpecial);
    if (!keyframes.length > 0) {
      return null;
    }
    console.log('emit');
    console.log(this._ee);
    this._ee.emitEvent(PrimitiveKeyframesManager.EVENTS.ON_SPECIAL_KEYFRAME,
      [keyframes[0]]);

    return keyframes[0];
  }

  _fireKeyframes(time) {
    if (this._keyframesQueue.length > 0) {
      const keyframesBeforeAndAtTime = this._keyframesQueue.filter(
        k => k.at <= time);
      keyframesBeforeAndAtTime.forEach((keyframe) => {
        this._ee.emitEvent(PrimitiveKeyframesManager.EVENTS.ON_KEYFRAME,
          [keyframe]);
      });
      return keyframesBeforeAndAtTime;
    }
    return [];
  }

  _mergeKeyframes() {
    const generalKeyframes = this._storyNodeKeyframes.general;
    let mergedKeyframes = [];
    if (generalKeyframes) {
      mergedKeyframes = generalKeyframes.map((keyframe) => {
        const localeKeyframeAtSameTime = this._storyNodeKeyframes.getLocaleSpecificKeyframeAtTime(
          this._languageCode, keyframe.at);
        if (!localeKeyframeAtSameTime) {
          return keyframe;
        }
        const mergedKeyframe = keyframe.clone();
        Object.assign(mergedKeyframe.data, localeKeyframeAtSameTime.data);
        mergedKeyframe.commands = localeKeyframeAtSameTime.commands;
        return mergedKeyframe;
      });

      mergedKeyframes.sort((a, b) => a.at - b.at);
    }

    const splittedKeyframes = mergedKeyframes.reduce(
      (splittedKeyframesAcc, curKeyframe) => {
        if (!curKeyframe.atSpecial) {
          splittedKeyframesAcc[0].push(curKeyframe);
        } else {
          splittedKeyframesAcc[1].push(curKeyframe);
        }
        return splittedKeyframesAcc;
      }, [[], []]);
    this._timedKeyframes = splittedKeyframes[0];
    this._specialKeyframes = splittedKeyframes[1];
    this._fillQueueWithTimedKeyframes();
  }

  _fillQueueWithTimedKeyframes() {
    this._keyframesQueue = this._timedKeyframes.splice(0);
  }

  resetKeyframes() {
    this._timedKeyframes.forEach(k => k.reset());
    this._specialKeyframes.forEach(k => k.reset());
    this._fillQueueWithTimedKeyframes();
  }

  getSpecialKeyframes() {
    return this._specialKeyframes;
  }

  getTimedKeyframes() {
    return this._timedKeyframes;
  }

  getKeyframesQueue() {
    return this._keyframesQueue;
  }

  addEventListener(name, callback) {
    this._ee.addListener(name, callback);
  }

  removeEventListener(event, callback) {
    if (event) {
      if (callback) {
        this._ee.removeListener(event, callback);
      } else {
        this._ee.removeEvent(event);
      }
    } else {
      this._ee.removeEvent('*');
    }
  }
}
