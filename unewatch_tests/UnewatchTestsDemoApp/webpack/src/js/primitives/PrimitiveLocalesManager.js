import EventEmitter from 'wolfy87-eventemitter';

import CONSTANTS from '../../test_cases/Backend3000/js/Constants';

export default class PrimitiveLocalesManager {
  static EVENTS = { // They should be used to trigger a repaint of the page
    LOCALES_CHANGED: 'localesChanged',
    LANGUAGE_CHANGED: 'languageChanged',
    STRINGS_MIGHT_HAVE_CHANGED: 'stringsMightHaveChanged'
  };

  static DEFAULT_SEPARATOR = '__';

  _locales = null;
  _language = CONSTANTS.DEFAULT_LANGUAGE;
  _ee = new EventEmitter();

  constructor(languageCode, locales) {
    if (!languageCode) {
      throw new Error('Language code is required.');
    }
    this.setLanguage(languageCode);

    if (!locales || typeof locales !== 'object') {
      throw new Error('Locales dict is required.');
    }

    this.setLocales(locales);
  }

  setLocales(localesObject) {
    this._locales = localesObject;// TODO: Maybe validate them before setting them
    this._ee.emitEvent(PrimitiveLocalesManager.EVENTS.LOCALES_CHANGED);
    this._ee.emitEvent(
      PrimitiveLocalesManager.EVENTS.STRINGS_MIGHT_HAVE_CHANGED);
  }

  stringExists(stringName, alsoDefault = false) {
    if (alsoDefault) {
      return this.defaultStringExists(stringName);
    }

    return !!this._getValuesForKey(stringName)[this._language];
  }

  getAllKeys() {
    return this._locales.map(l => l.key);
  }

  getAllLanguageCodesForKey(key) {
    return Object.keys(this._getValuesForKey(key));
  }

  _getValuesForKey(key) {
    const values = this._locales.filter(
      l => l.key === key);
    return values.length > 0 ? values[0].values : {};
  }

  _getDefaultKey(stringName) {
    return Object.keys(this._getValuesForKey(stringName))
                 .filter((key) => {
                   const keyArr = key.split(
                     PrimitiveLocalesManager.DEFAULT_SEPARATOR);
                   if (keyArr.length > 0) {
                     return keyArr.filter(v => v === 'default').length > 0;
                   }
                   return [''];
                 })[0];
  }

  defaultStringExists(stringName) {
    return !!this._getDefaultKey(stringName);
  }

  getDefault(stringName) {
    if (!this.defaultStringExists(stringName)) {
      return '';
    }
    return this._getValuesForKey(stringName)[this._getDefaultKey(
      stringName)];
  }

  get(stringName, language = this._language) {
    if (!this.stringExists(stringName)) {
      if (!this.defaultStringExists(stringName)) {
        return '';
      }
      // Return default if string for current language doesnt exist
      return this.getDefault(stringName);
    }

    // Return string if string name for current language exists
    return this._getValuesForKey(stringName)[language];
  }

  getOrSomething(stringName, defaultString, language = this._language) {
    return this.get(stringName, language) || defaultString;
  }

  setLanguage(languageCode) {
    if (languageCode !== this._language) {
      this._language = languageCode;

      this._ee.emitEvent(PrimitiveLocalesManager.EVENTS.LANGUAGE_CHANGED);
      this._ee.emitEvent(
        PrimitiveLocalesManager.EVENTS.STRINGS_MIGHT_HAVE_CHANGED);
    }
  }

  getLanguage() {
    return this._language;
  }

  addEventListener(event, callback) {
    this._ee.addListener(event, callback);
  }

  removeEventListener(event, callback) {
    if (event) {
      if (callback) {
        this._ee.removeListener(event, callback);
      } else {
        this._ee.removeEvent(event);
      }
    } else {
      this._ee.removeEvent('*');
    }
  }
}
