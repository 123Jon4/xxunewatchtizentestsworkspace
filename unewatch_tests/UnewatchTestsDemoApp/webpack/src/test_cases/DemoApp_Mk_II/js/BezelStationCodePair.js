import BezelCode, { BezelCodePart, BezelCodeDirection } from './BezelCode';

export default class BezelStationCodePair {
  constructor(stationId, bezelCode) {
    this.stationId = stationId;
    this.bezelCode = bezelCode;
  }

  static parseRawJsonObject(raw) {
    const id = raw.id;
    const parsedCodeParts = raw.code.split(';').map((x) => {
      const code = x.match(/([0-9])([A-Z])/); // Split the tick number and the direction string
      const ticks = parseFloat(code[1]);
      const direction = code[2];
      return new BezelCodePart(
        direction === 'L' ? BezelCodeDirection.CCW : BezelCodeDirection.CW,
        ticks
      );
    });
    const bezelCode = new BezelCode(parsedCodeParts);
    return new BezelStationCodePair(id, bezelCode);
  }
}
