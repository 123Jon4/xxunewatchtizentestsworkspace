export default class BezelCode {
  static DEFAULT_STRING_SEPARATOR = ';';

  constructor(bezelCodeParts) {
    this.bezelCodeParts = bezelCodeParts || [];
  }

  addCodePart(codePart) {
    this.bezelCodeParts.push(codePart);
  }

  setCodePart(codePart, index) {
    this.bezelCodeParts.splice(index, 0, codePart);
  }

  setCodeParts(arr) {
    this.bezelCodeParts = arr;
  }

  getCodeParts() {
    return this.bezelCodeParts;
  }

  getLatestCodePart() {
    return this.bezelCodeParts[this.bezelCodeParts.length - 1];
  }

  hasCodeParts() {
    return !!this.getLatestCodePart();
  }

  incrementLastPart() {
    if (this.bezelCodeParts.length > 0) {
      this.bezelCodeParts[this.bezelCodeParts.length - 1].ticks += 1;
    }
  }

  equals(other) {
    if (this.bezelCodeParts.length > 0 &&
      other.bezelCodeParts.length !== this.bezelCodeParts.length) {
      return false;
    }
    let stillMatching = true;
    for (let i = 0; i < this.bezelCodeParts.length && stillMatching; i += 1) {
      const bezelCodePart = this.bezelCodeParts[i];
      if (!bezelCodePart.equals(other.bezelCodeParts[i])) {
        stillMatching = false;
      }
    }

    return stillMatching;
  }

  contains(other) {
    if (other.bezelCodeParts.length > 0 &&
      this.bezelCodeParts.length < other.bezelCodeParts.length) {
      return false;
    }

    let stillMatching = true;
    for (let i = 0; i < other.bezelCodeParts.length && stillMatching; i += 1) {
      const otherBezelCodePart = other.bezelCodeParts[i];
      if (i < other.bezelCodeParts.length - 1) {
        const match = otherBezelCodePart.equals(this.bezelCodeParts[i]);
        if (!match) {
          stillMatching = false;
        }
      } else {
        stillMatching = this.bezelCodeParts[other.bezelCodeParts.length -
        1].contains(otherBezelCodePart);
      }
    }

    return stillMatching;
  }

  getLength() {
    return this.bezelCodeParts.length;
  }

  toString(separator = BezelCode.DEFAULT_STRING_SEPARATOR) {
    return this.bezelCodeParts.join(separator);
  }
}

export class BezelCodeDirection {
  static CCW = Symbol('CCW');
  static CW = Symbol('CW');
  static NONE = Symbol('NONE');

  static convertToJSON(direction) {
    switch (direction) {
      case BezelCodeDirection.CCW:
        return 'L';
      case BezelCodeDirection.CW:
        return 'R';
      default:
        return '';
    }
  }
}

export class BezelCodePart {
  constructor(dir, ticks) {
    this.dir = dir;
    this.ticks = ticks;
  }

  equals(other) {
    return other.dir === this.dir && other.ticks === this.ticks;
  }

  contains(other) {
    return other.dir === this.dir && other.ticks <= this.ticks;
  }

  toString() {
    return `${this.ticks}${BezelCodeDirection.convertToJSON(this.dir)}`;
  }
}
