/**
 * Any test case should get its own routes.
 */
import RouteGenerator from '../../../router/RouteGenerator';
import StandbyToArtefact from '../views/StandbyToArtefact';
import Standby from '../views/Standby/Standby';
import BezelInput from '../views/Standby/BezelInput';
import Artefact from '../views/Artefact/Artefact';

const routeG = new RouteGenerator('StandbyToArtefact');

export default {
  ...routeG.generate('/', '', StandbyToArtefact),
  children: [
    routeG.generate('', 'Standby', Standby),
    routeG.generate('/standby-bezel-input', 'Standby.BezelInput', BezelInput),
    routeG.generate('/artefact/:id', 'Artefact', Artefact, true)
  ]
};
