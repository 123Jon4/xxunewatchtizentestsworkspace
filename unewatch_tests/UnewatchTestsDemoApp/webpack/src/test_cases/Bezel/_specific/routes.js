/**
 * Any test case should get their own routes.
 */
//import BezelInput from '../BezelInput';
import BezelInputAdvanced from '../BezelInputAdvanced';
import RouteGenerator from '../../../router/RouteGenerator';

const routeG = new RouteGenerator('Bezel');
export default {
  ...routeG.generate('/', '', BezelInputAdvanced)
};
