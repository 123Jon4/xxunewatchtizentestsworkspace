/**
 * Any test case should get its own routes.
 */
import RouteGenerator from '../../../router/RouteGenerator';
import Backend3000 from '../Backend3000';
import JsonGetTest from '../views/_test/JsonGetTest';
import PlaybackTest from '../views/PlaybackTest/PlaybackTest';

const routeG = new RouteGenerator('Backend3000');

export default {
  ...routeG.generate('/', '', Backend3000),
  children: [
    routeG.generate('', 'PlaybackTest', PlaybackTest),
    routeG.generate('/json-get-test', 'JsonTest', JsonGetTest)

    /*
     {
     path: '/setup',
     name: 'Backend.Setup',
     component: Setup
     },
     {
     path: '/splashscreen',
     name: 'Backend.Splashscreen',
     component: Splashcreen
     }
     */
  ]
};
