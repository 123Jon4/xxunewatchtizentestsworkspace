export const LANGUAGE_CODE = {
  DE: 'de',
  EN: 'en',
  DA: 'da'
};

export const DEFAULT_LANGUAGE = LANGUAGE_CODE.DE;

export const ROUTE_NAME = {
  // ...
};

const Constants = {
  LANGUAGE_CODE,
  ROUTE_NAME
};
export default Constants;
