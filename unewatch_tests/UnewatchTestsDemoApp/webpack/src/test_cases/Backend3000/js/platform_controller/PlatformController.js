/**
 * - Events
 *  - OnBezel
 *  - OnBezelStopped
 *  - OnDeviceBatteryChange
 *  - OnHeadsetBatteryChange
 *  - OnHeadsetConnected
 *  - OnHeadsetDisconnected
 *  - OnHeadsetCallButtonPressed
 *  - OnHeadsetVolumeChange
 *  - OnNfcTag | Not implemented
 * - enableKioskMode
 * - disableKioskMode
 * - setSystemVolume
 * - getSystemVolume
 * - getSystemVersion
 * - getSystemFreeMemory
 * - resolvePath
 * - setScreenState
 * - getScreenState
 * -
 */

import EventEmitter from 'wolfy87-eventemitter';

export default class PlatformController {
  _ee = new EventEmitter();
  platformControllerBridge = null;

  constructor(platformControllerBridge) {
    this.platformControllerBridge = platformControllerBridge;
  }

  // Do I really need this class???
}
