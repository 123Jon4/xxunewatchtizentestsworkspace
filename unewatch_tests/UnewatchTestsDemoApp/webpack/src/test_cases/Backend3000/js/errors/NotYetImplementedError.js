export default class NotYetImplementedError extends Error {
  constructor() {
    super('This function is not yet implemented.');
    // Source: http://azimi.me/2015/09/23/high-custom-error-class-es6.html
    this.name = NotYetImplementedError.name; // Sets the name of the error
    // Source: https://medium.com/@xjamundx/custom-javascript-errors-in-es6-aa891b173f87
    Error.captureStackTrace(this, NotYetImplementedError); // Removes this class from error stack
  }
}
