// TODO: Create a super class for both parsers
import StationGroup from './StationGroup';

export default class StationsParser {
  _allowedScriptVersions = ['1.0'];
  _currentJsonVersion = '';
  _rawJson = null;

  _groups = {};

  parse(rawJson) {
    // Parse and verify JSON and set valid station nodes
    // TODO: Check validity of properties
    // TODO: Throw an Error if something is wrong
    this._rawJson = rawJson;
    this._currentJsonVersion = rawJson.header.version;

    if (this._allowedScriptVersions.indexOf(this._currentJsonVersion) === -1) {
      throw new Error(`JSON with version number${this._currentJsonVersion
        } is not compatible with this version of StationsParser (${this._allowedScriptVersions.join(
        ',')}).`);
    }

    const content = rawJson.content;
    const groups = {};

    content.forEach(
      (curGroup) => {
        const group = StationGroup.createFromJSON(curGroup);
        groups[group.id] = group;
      });

    const locales = StationsParser._extractLocalesOutOfGroups(
      Object.values(groups));

    this._groups = groups;
    this._locales = locales;
  }

  static _extractLocalesOutOfGroups(groupsArr) {
    const localesDict = {};
    groupsArr.forEach((group) => {
      localesDict[group.id] = group.locales;
      Object.values(group.subStations).forEach((station) => {
        localesDict[station.getCompositeKey()] = station.locales;
      });
    });

    return localesDict;
  }

  getGroups() {
    return this._groups;
  }

  getLocales() {
    return this._locales;
  }

  getRawJson() {
    return this._rawJson;
  }
}
