import JSONRepresentingObject from '../../../JSONRepresentingObject';

export default class StoryNode extends JSONRepresentingObject {
  static COMPOSITE_KEY_SEPARATOR = '.';

  id = 0;
  comment = '';
  data = {};
  commands = { startView: { name: '', params: {} } };
  locales = [];

  group;

  getCompositeKey() {
    return `${this.group.id}${StoryNode.COMPOSITE_KEY_SEPARATOR}${this.id}`;
  }

  static createFromJSON(json) {
    const node = new StoryNode();
    node.id = json.id;
    node.comment = json.comment;
    node.data = json.data;
    node.locales = json.locales;
    Object.assign(node.commands, json.commands);
    return node;
  }
}
