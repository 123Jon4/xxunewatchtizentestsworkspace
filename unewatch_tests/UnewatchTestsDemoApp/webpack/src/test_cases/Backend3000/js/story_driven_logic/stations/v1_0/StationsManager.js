import TypeSafety from '../../../helpers/TypeSafety';
import Station from './Station';
import PrimitiveLocalesManager from '../../../../../../js/primitives/PrimitiveLocalesManager';

export default class StationsManager {
  _stationGroups = {};
  _locales = {};
  _currentStation;
  _stationsHistory = [];

  constructor(stationGroups, stationsLocales) {
    this._stationGroups = stationGroups;
    this._locales = stationsLocales;
  }

  hasLocales(groupId, stationId) {
    return !!this._getLocales(groupId, stationId);
  }

  _getLocales(groupId, stationId) {
    if (!stationId) {
      if (!this.groupExists(groupId)) {
        return null;
      }
      return this._locales[groupId];
    }

    if (!this.stationExists(groupId, stationId)) {
      return null;
    }

    return this._locales[this.getStation(groupId, stationId)
                             .getCompositeKey()];
  }

  getAllLocales() {
    return this._locales;
  }

  createLocalesManagerOfStation(languageCode, groupId, stationId) {
    return new PrimitiveLocalesManager(languageCode,
      this._getLocales(groupId, stationId));
  }

  createLocalesManagerOfGroup(languageCode, groupId) {
    return new PrimitiveLocalesManager(languageCode, this._getLocales(groupId));
  }

  getLocalesManagersForAll(languageCode) {
    const newLocalesDict = {};
    Object.entries(this._locales).forEach((curLocale) => {
      newLocalesDict[curLocale[0]] = new PrimitiveLocalesManager(languageCode,
        curLocale[1] || []);
    });
    return newLocalesDict;
  }

  open(groupId, stationId) {
    if (this.stationExists(groupId, stationId)) {
      this._setCurrentStation(this.getStation(groupId, stationId));
      return this.getCurrentStation();
    }

    return null;
  }

  closeCurrentStation(visited = true) {
    this._addCurrentStationToHistory();
    this._currentStation.visited = visited;
    this._currentStation = null;
  }

  stationExists(groupId, stationId) {
    return !!this.getStation(groupId, stationId);
  }

  groupExists(groupId) {
    return !!this.getStationGroup(groupId);
  }

  getStation(groupId, stationId) {
    const group = this._stationGroups[groupId];
    if (!group) {
      return null;
    }
    const station = group.subStations[stationId];
    return station || null;
  }

  getStationGroup(groupId) {
    const group = this._stationGroups[groupId];
    return group || null;
  }

  getAllStations() {
    return Object.values(this._stationGroups).reduce((arr, group) => {
      return arr.concat(Object.values(group.subStations));
    }, []);
  }

  getStationGroups() {
    return Object.values(this._stationGroups);
  }

  _setCurrentStation(station) {
    this._currentStation = station;
  }

  hasCurrentStation() {
    return !!this._currentStation;
  }

  getCurrentStation() {
    TypeSafety.brutalTypeChecking(this._currentStation, Station);
    return this._currentStation;
  }

  _addCurrentStationToHistory() {
    this._addToHistory(this._currentStation);
  }

  _addToHistory(station) {
    this._stationsHistory.push(station);
  }
}
