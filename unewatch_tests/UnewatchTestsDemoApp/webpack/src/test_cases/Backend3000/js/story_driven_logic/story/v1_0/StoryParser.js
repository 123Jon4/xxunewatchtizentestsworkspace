import Story from './Story';
import StoryNodeGroup from './StoryNodeGroup';
import StoryFlowControlNode from './StoryFlowControlNode';
import StoryNodeKeyframes from './StoryNodeKeyframes';

export default class StoryParser {
  _allowedScriptVersions = ['1.0'];
  _currentJsonVersion = '';
  _story;

  parse(nodesJsonRaw, flowJsonRaw, keyframeJsonRaw) {
    const jsonVersions = [
      nodesJsonRaw.header.version,
      flowJsonRaw.header.version,
      keyframeJsonRaw.header.version];

    // Source: http://www.jstips.co/en/javascript/deduplicate-an-array/
    const jsonVersion = jsonVersions.filter(
      (el, i, arr) => arr.indexOf(el) === i);
    if (jsonVersion.length !== 1) {
      throw new Error('The provided JSONs have different version numbers.');
    }

    this._currentJsonVersion = jsonVersion[0];
    if (this._allowedScriptVersions.indexOf(this._currentJsonVersion) === -1) {
      throw new Error(`JSON with version number${this._currentJsonVersion
        } is not compatible with this version of StoryParser (${this._allowedScriptVersions.join(
        ',')}).`);
    }

    const nodesJsonContent = nodesJsonRaw.content;
    const flowJsonContent = flowJsonRaw.content;
    const keyframesJsonContent = keyframeJsonRaw.content;

    const groups = StoryParser._parseGroups(nodesJsonContent);
    const locales = StoryParser._extractLocalesOutOfGroups(
      Object.values(groups));
    const flow = StoryParser._parseFlow(flowJsonContent);
    const allKeyframes = StoryParser._parseKeyframes(keyframesJsonContent);

    this._story = new Story(groups, flow, allKeyframes, locales);
  }

  static _parseGroups(content) {
    const groups = {};
    content.forEach((curGroup) => {
      const group = StoryNodeGroup.createFromJSON(curGroup);
      groups[group.id] = group;
    });
    return groups;
  }

  static _parseFlow(content) {
    const flow = {};
    content.forEach((curFlowControlNode) => {
      const flowControlNode = StoryFlowControlNode.createFromJSON(
        curFlowControlNode);
      flow[flowControlNode.nodeId] = flowControlNode;
    });

    return flow;
  }

  static _parseKeyframes(content) {
    const keyframesOfNodes = {};
    content.forEach((curKeyframes) => {
      const keyframes = StoryNodeKeyframes.createFromJSON(curKeyframes);
      keyframesOfNodes[keyframes.nodeId] = keyframes; // eslint-disable-line no-param-reassign
    });

    return keyframesOfNodes;
  }

  static _extractLocalesOutOfGroups(groupsArr) {
    const localesDict = {};
    groupsArr.forEach((group) => {
      localesDict[group.id] = group.locales;
      Object.values(group.subNodes).forEach((node) => {
        localesDict[node.getCompositeKey()] = node.locales;
      });
    });

    return localesDict;
  }

  getStory() {
    return this._story;
  }
}
