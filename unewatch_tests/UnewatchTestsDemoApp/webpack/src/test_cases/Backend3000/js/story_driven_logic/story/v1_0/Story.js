/**
 * This is only a container for the 3 parts of the story (groups, nodes and trigger map)
 */
import StoryNode from './StoryNode';
import TypeSafety from '../../../helpers/TypeSafety';

export default class Story {
  _groups = {};
  _keyframes = {};
  _locales = {};
  _flow = [];

  constructor(groups, flow, keyframes, locales) {
    if (groups && flow && keyframes && locales) {
      this._groups = groups;
      this._keyframes = keyframes;
      this._locales = locales;
      this._flow = flow;
    } else {
      throw new Error('All parameters have to be defined');
    }
  }

  getGroups() {
    return this._groups;
  }

  nodeExists(groupId, nodeId) {
    return !!this.getNode(groupId, nodeId);
  }

  getNodeByCompositeKey(compositeKey) {
    TypeSafety.brutalTypeCheckingString(compositeKey, 'string');
    const ids = compositeKey.split(StoryNode.COMPOSITE_KEY_SEPARATOR);
    return this.getNode(ids[0], ids[1]);
  }

  getNodesInARowFrom(compositeKey) {
    const nodesInARow = [];
    let currentNode = this.getNodeByCompositeKey(compositeKey);
    let flowControlNode = this.getFlowControlNodeByNode(currentNode);
    nodesInARow.push(currentNode);
    while (flowControlNode && flowControlNode.hasDefaultPath()) {
      currentNode = this.getNodeByCompositeKey(
        flowControlNode.getDefaultPath().nodeId);
      flowControlNode = this.getFlowControlNodeByNode(currentNode);
      nodesInARow.push(currentNode);
    }
    return nodesInARow;
  }

  getNode(groupId, nodeId) {
    const group = this._groups[groupId];
    return group ? group.subNodes[nodeId] : null;
  }

  getKeyframes() {
    return this._keyframes;
  }

  getKeyframesForNode(groupId, nodeId) {
    return this._keyframes[this.getNode(groupId, nodeId).getCompositeKey()];
  }

  getFlow() {
    return this._flow;
  }

  hasFlowControlNode(groupId, nodeId) {
    return !!this.getFlowControlNode(groupId, nodeId);
  }

  getFlowControlNodeByNode(node) {
    return this.getFlowControlNode(node.group.id, node.id);
  }

  getFlowControlNode(groupId, nodeId) {
    const fcn = this._flow[this.getNode(groupId, nodeId).getCompositeKey()];
    return fcn || null;
  }

  hasTrigger(triggerName) {
    return this.getFlowControlNodeWithTrigger(triggerName).length > 0;
  }

  getFlowControlNodeWithTrigger(triggerName) {
    const fcns = Object.values(this._flow)
                       .filter(fcn => fcn.nodeTrigger === triggerName);
    return fcns.length > 0 ? fcns[0] : null;
  }

  getNodeWithTrigger(triggerName) {
    return this.getNodeByCompositeKey(
      this.getFlowControlNodeWithTrigger(triggerName).nodeId);
  }
}
