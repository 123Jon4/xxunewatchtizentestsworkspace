import JSONRepresentingObject from '../../../JSONRepresentingObject';

export default class StoryFlowControlNode extends JSONRepresentingObject {
  // id=0; // Someday this might be useful
  nodeId = 0;
  nodeTrigger = '';
  paths = [];
  chosenPathName = '';

  hasNodeTrigger() {
    return this.nodeTrigger !== '';
  }

  hasPaths() {
    return this.paths.length > 0;
  }

  hasMultiplePaths() {
    return this.paths.length > 1;
  }

  getPaths() {
    return this.paths;
  }

  hasPath(pathName) {
    return !!this.getPath(pathName);
  }

  getPath(pathName) {
    const paths = this.getPaths().filter(path => path.name === pathName);
    return paths.length > 0 ? paths[0] : null;
  }

  hasDefaultPath() {
    return !!this.getDefaultPath();
  }

  getDefaultPath() {
    const paths = this.paths.filter(path => path.name === 'default');
    return paths.length > 0 ? paths[0] : null;
  }

  static createFromJSON(json) {
    const flowControlNode = new StoryFlowControlNode();
    flowControlNode.nodeId = json.nodeId;
    flowControlNode.nodeTrigger = json.nodeTrigger;
    const paths = [];
    (json.paths || []).forEach(
      (curPath) => {
        const path = Path.createFromJSON(curPath);
        paths.push(path);
      });
    flowControlNode.paths = paths;

    return flowControlNode;
  }
}

export class Path extends JSONRepresentingObject {
  name = 'default';
  nodeId = 0;
  requirements = [];
  specialCommand = '';

  hasRequirements() {
    return this.requirements.length > 0;
  }

  hasRequirement(name) {
    return !!this.getRequirement(name);
  }

  getRequirement(name) {
    const reqs = this.requirements.filter(req => req.name = name);
    return reqs.length > 0 ? reqs[0] : null;
  }

  static createFromJSON(json) {
    const path = new Path();
    path.name = json.name;
    path.nodeId = json.nodeId;
    path.specialCommand = json.specialCommand;
    path.requirements = [];
    (json.requirements || []).forEach(
      (curReq) => {
        const req = PathRequirement.createFromJSON(curReq);
        path.requirements.push(req);
      });
    return path;
  }
}

export class PathRequirement extends JSONRepresentingObject {
  name = '';
  params = {};

  hasParam() {
    return Object.entries(this.params).length > 0;
  }

  static createFromJSON(json) {
    const req = new PathRequirement();
    req.name = json.name;
    req.params = json.params;
    return req;
  }
}
