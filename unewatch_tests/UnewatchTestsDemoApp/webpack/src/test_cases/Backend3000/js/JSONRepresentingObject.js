export default class JSONRepresentingObject {
  // The constructor should be private.
  static createFromJSON(json) {
    throw new Error('Override this function.');
  }

  toJSON() {
    return JSON.parse(this);
  }
}
