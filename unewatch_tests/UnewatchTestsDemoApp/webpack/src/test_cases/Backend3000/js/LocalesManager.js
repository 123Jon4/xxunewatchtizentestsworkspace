/**
 * - setRawJsonObject | set the raw json object, parses and validates it
 * - get | Get locale string for current language without checking if it exists and return null if not
 * - getOrDefault | Like get but returns a default string if no locale found
 * - has | Check if there is a locale string for the current language code
 * - setLanguage | Set ISO language code
 * - getLanguage
 */
