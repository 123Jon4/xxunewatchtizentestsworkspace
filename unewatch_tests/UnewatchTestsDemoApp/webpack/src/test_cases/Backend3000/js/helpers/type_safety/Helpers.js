export default class Helpers {
  static getSymbolDescription(symbol) {
    return symbol.toString().slice(7, -1) || '';
  }
}
