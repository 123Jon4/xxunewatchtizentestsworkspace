import TypeSafeValue from './TypeSafeValue';
import ValueTypes from './ValueTypes';

export default class TypeChecker {
  static check(value, type) {
    if (value === undefined || !(type instanceof Function)) {
      return false;
    }
    const typeSafeValue = new TypeSafeValue(value);
    return typeSafeValue.isPrimitive() ? ValueTypes.compareTypeOfPrimitives(
      value, type) : value instanceof type;
  }

  static ensure(value, type, customErrorText) {
    if (!TypeChecker.check(value, type)) {
      const valueTypeName = ValueTypes.getTypeName(value);
      const typeTypeName = ValueTypes.getTypeName(type);
      if (customErrorText && TypeChecker.check(customErrorText, String)) {
        throw new Error(customErrorText);
      } else {
        throw new Error(
          `The type of the value '${valueTypeName}' is not equal to the expected type '${typeTypeName}'.`);
      }
    }

    return true;
  }
}
