//import axios from 'axios';

export default class HttpClient {
  static getAsJson(url) {
    /*
     // FIXME:This does not work on emulator and device
     axios.get(url, {
     responseType: 'json',
     validateStatus(status) {
     console.log(status);
     return true; //status === 200 || status === 0; // Accept only if the status code is 200 on a server or 0 when using file protocol
     }
     });
     */
    return new Promise((resolve, reject) => {
      HttpClient.createClient(url)
                .then((response) => {
                  try {
                    //console.log(response)
                    resolve(JSON.parse(response));
                  } catch (e) {
                    reject(e);
                  }
                })
                .catch(e => reject(e));
    });
  }

  static createClient(url) {
    return new Promise((resolve, reject) => {
      const xhr = new XMLHttpRequest();
      xhr.open('GET', url);
      xhr.onreadystatechange = () => {
        if (xhr.readyState === 4 && (xhr.status === 200 || xhr.status === 0)) {
          // To allow retrieving of local files (file:// protocol) check for status code 0
          resolve(xhr.responseText);
        }
      };
      xhr.onerror = (e) => {
        reject(e);
      };
      xhr.send();
    });
  }
}
