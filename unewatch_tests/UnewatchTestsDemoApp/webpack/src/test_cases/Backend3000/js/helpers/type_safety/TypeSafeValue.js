import ValueTypes from './ValueTypes';
import Null from './Null';
import TypeChecker from './TypeChecker';

export default class TypeSafeValue {
  _value = null; // Put primitive types into their respective Class representations
  _type = Object; // only classes (instanceof Object) allowed

  constructor(value) {
    // Automatically determine the type of the value and create an instance of TypeSafeValue with this type information
    const type = ValueTypes.determineType(value);
    this._setValue(value, type);
  }

  static parseValue(rawValue) {
    return new TypeSafeValue(rawValue);
  }

  _setValue(value, type = Object) {
    this._value = value;
    this._type = type;
  }

  getValue() {
    // Dont't check the type now. This can be done manually before getting the value.
    return this._value;
  }

  get(type) {
    if (!type) {
      return this.getValue();
    }
    return TypeChecker.ensure(this.getValue(), type) ? this.getValue() : null; // Null will not be returned because ensure will throw an error if tyoes not equal
  }

  getType() {
    return this._type;
  }

  isPrimitive() {
    return ValueTypes.determineHigherType(this._value) ===
      ValueTypes.HIGHER_TYPES.PRIMITIVE;
  }

  isString() {
    return this.isOfType(String);
  }

  isNumber() {
    return this.isOfType(Number);
  }

  isBoolean() {
    return this.isOfType(Boolean);
  }

  isArray() {
    return this.isOfType(Array);
  }

  isObject() {
    return this.isOfType(Object);
  }

  isClass() {
    return ValueTypes.determineHigherType(this._value) ===
      ValueTypes.HIGHER_TYPES.CLASS;
  }

  isNull() {
    return this.isType(Null);
  }

  isType(type) {
    return ValueTypes.isType(type) && this._type === type;
  }

  isOfType(type) {
    return ValueTypes.isType(type) && this._type instanceof type;
  }

  getNotNull(defaultValue) {
    if (!this.typeEquals(defaultValue)) {
      throw new Error('Types are not equal.');
    }
    return !this.isNull() ? this._value : defaultValue;
  }

  getOrSomething(defaultValue) {
    if (!this.typeEquals(defaultValue)) {
      throw new Error('Types are not equal.');
    }
    return !this.isNull() ? this._value : defaultValue;
  }

  typeEquals(type) {
    return this.isType(type);
  }
}
