import TypeSafeContainer from './TypeSafeContainer';
import ValueTypes from './ValueTypes';

export default class TypeSafeDictionary extends TypeSafeContainer {
  _keyType = String;
  _valueType = String;
  dict = {};

  constructor(valueType, startDict, keyType = String) {
    super();
    if (!ValueTypes.isType(valueType) || !ValueTypes.isType(keyType)) {
      throw Error('These are no types');
    }
    if (startDict) {

    }
  }

  _checkType(value) {

  }

  _checkAllElements() {

  }
}
