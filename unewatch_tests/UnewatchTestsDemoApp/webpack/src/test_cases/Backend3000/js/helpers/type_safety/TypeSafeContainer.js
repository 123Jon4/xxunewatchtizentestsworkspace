import TypeChecker from './TypeChecker';
import ValueTypes from './ValueTypes';

export default class TypeSafeContainer {
  _checkType(value) {
    return false;
  }

  static checkAllArrayElements(arr, type) {
    let equalTypes = true;
    for (let i = 0; i < arr.length && equalTypes; i += 1) {
      equalTypes = equalTypes && TypeChecker.ensure(arr[i], type,
        `The type of the value at index ${i} is not equal to ${ValueTypes.getTypeName(
          type)}`);
    }
    return equalTypes;
  }

  static checkAllDictElements(dict, valueType, keyType = String) {
    let equalTypes = true;
    const entries = Object.entries(dict);
    for (let i = 0; i < entries.length && equalTypes; i += 1) {
      const key = entries[i][0];
      const value = entries[i][1];
      equalTypes = equalTypes && (TypeChecker.ensure(key, keyType,
        `The type of the key '${key}' is unequal to '${ValueTypes.getTypeName(
          keyType)}'`) &&
        TypeChecker.ensure(value, valueType,
          `The type '${ValueTypes.getTypeName(
            value)}' of the value at key '${key}' is unequal to '${ValueTypes.getTypeName(
            valueType)}'`));
    }
    return equalTypes;
  }
}
