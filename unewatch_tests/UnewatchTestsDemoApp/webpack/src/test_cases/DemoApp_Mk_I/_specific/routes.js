/**
 * Any test case should get its own routes.
 */
import RouteGenerator from '../../../router/RouteGenerator';
import UnewatchOS from '../views/UnewatchOS/UnewatchOS';
import Oldschool from '../views/Oldschool/Oldschool';
import Splashscreen from '../views/Splashscreen/Splashscreen';
import DemoApp from '../views/DemoApp';
import FakeSetup from '../views/Setup/FakeSetup';

const routeG = new RouteGenerator('DemoApp');

export default {
  ...routeG.generate('/', '', DemoApp),
  children: [
    routeG.generate('', 'UnewatchOS', UnewatchOS),
    routeG.generate('/setup', 'Setup', FakeSetup),
    routeG.generate('/splashscreen', 'Splashscreen', Splashscreen),
    routeG.generate('/oldschool/intro/:lang', 'Oldschool', Oldschool, true) // PartKey of Intro
  ]
};
