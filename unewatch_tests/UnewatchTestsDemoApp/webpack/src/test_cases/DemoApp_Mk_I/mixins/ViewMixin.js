const mixinName = 'ViewMixin'; // Change after renaming file

const ViewMixin = {
  props: ['container'],
  usesMixin(name) {
    if (!this.mixinNames) {
      this.mixinNames = [];
    }

    return this.mixinNames.indexOf(name) > -1;
  },
  isView: true,
  created() {
    console.log('Created: ViewMixin');

    if (!this.$options.usesMixin(mixinName)) {
      this.$options.mixinNames.push(mixinName);
    }
    console.log(this.$options.mixinNames);

    if (!this.container) {
      throw new Error('No Container injected.');
    } else {
      // Do something
    }
  },
  methods: {
    init() {
    },
    unload() {

    }
  },
  mounted() {
    const pathParts = this.$options.__file.split('\\');
    this.componentName = pathParts[pathParts.length - 1].split('.')[0];

    this.init();
  },
  beforeDestroy() {
    this.unload();
  }
};

export default ViewMixin;

export function isView(vueComponent) {
  return !!vueComponent.$options.isView;
}
