/**
 * Any test case should get its own routes.
 */
import RouteGenerator from '../../../router/RouteGenerator';
import SmoothInteraction from '../SmootherInteraction';
import TouchEvent from '../views/TouchEvent';

const routeG = new RouteGenerator('SmoothInteraction');

export default {
  ...routeG.generate('/', '', SmoothInteraction),
  children: [
    routeG.generate('', 'TouchEvent', TouchEvent)
  ]
};
