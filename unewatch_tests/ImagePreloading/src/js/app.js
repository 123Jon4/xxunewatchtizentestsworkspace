var EXECUTION_DELAY = 0;

function main() {
	var content = document.getElementById('dynamic-content');
	content.textContent = 'Loading...';

	/*
	 * preloadCustom(content, function(src) { var img = new Image(); img.onload =
	 * function() { onImageLoaded(content, src); }; img.src = src; });
	 */

	preloadPreloadJs(content, function(src) {
		onImageLoaded(content, src);
	});

	/*
	 * preloadImageNative(content, function(src) { onImageLoaded(content, src);
	 * });
	 */
	// onImageLoaded(content, "./img/splashscreen_cover_1.jpg")
}

function onImageLoaded(content, src) {
	while (content.firstChild) {
		content.removeChild(content.firstChild);
	}
	//alert('Image loaded and ready to show.')
	var imgElement = document.createElement('img');
	imgElement.style.minWidth = '360px';
	imgElement.src = src;

	content.appendChild(imgElement);
}

function preloadCustom(content, loadedCallback) {
	var loader = new CustomPreloader("./img/splashscreen_cover_1.jpg",
			function(blobUrl) {
				loadedCallback(blobUrl);
			});
}

function preloadPreloadJs(content, loadedCallback) {
	var preload = new createjs.LoadQueue();
	preload.addEventListener("fileload", function(evt) {
		console.log(evt);
		loadedCallback(evt.result.src);
	});
	preload.loadFile("./img/splashscreen_cover_1.jpg");
}

function preloadImageNative(content, loadedCallback) {
	var img = new Image();
	img.onload = function() {
		loadedCallback(img.src);
	}
	img.src = './img/splashscreen_cover_1.jpg';
}

setTimeout(function() {
	main();
}, EXECUTION_DELAY);