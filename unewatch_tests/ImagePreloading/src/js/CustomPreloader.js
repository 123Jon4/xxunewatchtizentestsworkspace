// Source: http://dinbror.dk/blog/how-to-preload-entire-html5-video-before-play-solved/
var CustomPreloader = function(uri, onLoadCallback, onErrorCallback) {
	var xhr = new XMLHttpRequest();
	xhr.open('GET', uri, true);
	xhr.responseType = 'blob';

	xhr.onload = function() {
		console.log(this.status)
		if (this.status !== 200 && this.status !== 0) {
			if (onErrorCallback)
				onErrorCallback();
			return;
		}
		onLoadCallback(URL.createObjectURL(this.response));
	}
	xhr.onerror = function() {
		if (onErrorCallback)
			onErrorCallback();
	}

	xhr.send();
};