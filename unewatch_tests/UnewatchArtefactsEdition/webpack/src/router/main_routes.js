import RouteGenerator from './RouteGenerator';
import UnewatchOS from '../views/UnewatchOS/UnewatchOS';
import Splashscreen from '../views/Splashscreen/Splashscreen';
import Setup from '../views/Setup/Setup';
import Standby from '../views/Standby/Standby';
import BezelInput from '../views/Standby/BezelInput';
import UniversalApp from '../UniversalApp';
import Station from '../views/Station/Station';

export const name = 'UnewatchArtefactsEdition_MainRoutes';
const routeG = new RouteGenerator(name);

export default {
  ...routeG.generate('/', '', UniversalApp),
  children: [
    routeG.generate('', 'UnewatchOS', UnewatchOS),
    routeG.generate('/setup/', 'Setup', Setup),
    routeG.generate('/splashscreen', 'Splashscreen', Splashscreen),
    routeG.generate('/standby', 'Standby', Standby),
    routeG.generate('/standby-bezel-input', 'StandbyBezelInput', BezelInput),
    routeG.generate('/station/:compositeKey', 'Station', Station, true)
  ]
};
