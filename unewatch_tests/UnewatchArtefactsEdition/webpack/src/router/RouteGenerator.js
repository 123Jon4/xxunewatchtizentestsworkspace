export default class RouteGenerator {
  constructor(routesPrefix) {
    this.routesPrefix = routesPrefix;
  }

  generate(path, name, component, useProps) {
    return {
      path,
      name: `${name && name.length > 0 ? `${this.routesPrefix}_${name}` : ''}`,
      component,
      props: !!useProps
    };
  }
}
