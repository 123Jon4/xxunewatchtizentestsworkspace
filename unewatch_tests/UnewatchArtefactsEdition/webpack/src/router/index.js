import Vue from 'vue';
import Router from 'vue-router';
import MainRoutes from './main_routes';

Vue.use(Router);

export default new Router({
  routes: [MainRoutes]
});
