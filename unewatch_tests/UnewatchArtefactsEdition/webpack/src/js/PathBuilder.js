export default class PathBuilder {
  _rootPath = '';

  constructor(rootPath) {
    this._rootPath = rootPath;
  }

  getPathOfAudioForStation(station, languageCode) {
    return `${this._rootPath}_files/stations/${station.group.id} - ${station.group.name}/${station.id}_${languageCode}.mp3`;
  }

  getPathOfImageForStation(station) {

  }

  getPathOfAudioForStoryNode(node) {

  }
}
