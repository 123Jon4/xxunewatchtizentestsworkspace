import HttpClient from './helpers/HttpClient';

export default class JsonRetriever {
  static _rootPath = 'static/_json/';
  static _storyRootPath = 'story/';
  static _stationsRootPath = 'stations/';

  static STORY_JSONS = {
    STORY_FLOW: 'story_flow',
    STORY_KEYFRAMES: 'story_keyframes',
    STORY_NODES: 'story_nodes'
  };

  static STATIONS_JSONS = {
    STATIONS: 'stations',
    STATIONS_BEZEL_CODES: 'stations_bezel_codes',
    STATIONS_KEYFRAMES: 'stations_keyframes',
    STATIONS_NFC_CODES: 'stations_nfc_codes'
  };

  static OTHER_JSONS = {
    FILES: 'files',
    LOCALES: 'locales',
    RADIO: 'radio',
    SETTINGS: 'settings'
  };

  static getStations(path) {
    return JsonRetriever._get(JsonRetriever._stationsRootPath + path);
  }

  static getStory(path) {
    return JsonRetriever._get(JsonRetriever._storyRootPath + path);
  }

  static getOther(path) {
    return JsonRetriever._get(path);
  }

  static _get(path) {
    return HttpClient.getAsJson(`${JsonRetriever._rootPath + path}.json`);
  }
}
