import BezelCode from './BezelCode';
import StationsCodeProvider from '../StationsCodeProvider';

export default class BezelStationCodeProvider extends StationsCodeProvider {
  constructor() {
    super();
    // Pairs
    this._codePairs = [];
    this._maxCodeParts = -1;
    this._currentMatchingCodePairs = [];

    // Input
    this._currentBezelInput = new BezelCode();
  }

  setCodePairs(codePairs) {
    this._codePairs = codePairs;
    this.reset();
    this._maxCodeParts = codePairs.reduce((maxLength, bezelCodePair) => {
      const currentLength = bezelCodePair.bezelCode.getLength();
      return currentLength > maxLength ? currentLength : maxLength;
    }, 0);
  }

  getMaxCodeLength() {
    return this._maxCodeParts;
  }

  setBezelInput(bezelCode) {
    this._currentBezelInput = bezelCode;
    this._matchCodeParts();
  }

  getBezelInput() {
    return this._currentBezelInput;
  }

  checkForDuplicates() {
    // TODO: Check if a bezelCode was used more than once, but do this in a Parser class
  }

  reset() {
    this._currentMatchingCodePairs = this._codePairs;
    this._currentBezelInput = new BezelCode();
  }

  getSingleMatchingId() {
    return this.getFullyMatchingIds()[0];
  }

  getFullyMatchingIds() {
    return this._currentMatchingCodePairs
               .filter(x => x.bezelCode.equals(this.getBezelInput()))
               .map(x => x.stationId);
  }

  getMatchingIds() {
    return this._currentMatchingCodePairs.map(x => x.stationId);
  }

  getMatchingCodePairs() {
    return this._currentMatchingCodePairs;
  }

  hasOneMatchingId(fully) {
    return fully
      ? this.getFullyMatchingIds().length === 1
      : this.getMatchingIds().length === 1;
  }

  hasMatchingIds(fully) {
    return fully
      ? this.getFullyMatchingIds().length > 0
      : this.getMatchingIds().length > 0;
  }

  _matchCodeParts() {
    this._currentMatchingCodePairs = this._currentMatchingCodePairs.filter(x =>
      x.bezelCode.contains(this.getBezelInput())
    );
  }
}
