import BezelCode, { BezelCodePart, BezelCodeDirection } from './BezelCode';
import JSONRepresentingObject from '../../JSONRepresentingObject';

export default class BezelStationCodePair extends JSONRepresentingObject {
  constructor(stationId, bezelCode) {
    super();
    this.stationId = stationId;
    this.bezelCode = bezelCode;
  }

  static createFromJSON(raw) {
    const id = raw.id;
    const parsedCodeParts = raw.code.split(';').map((x) => {
      const code = x.match(/([0-9])([A-Z])/); // Split the tick number and the direction string
      const ticks = parseFloat(code[1]);
      const direction = code[2];
      return new BezelCodePart(
        direction === 'L' ? BezelCodeDirection.CCW : BezelCodeDirection.CW,
        ticks
      );
    });
    const bezelCode = new BezelCode(parsedCodeParts);
    return new BezelStationCodePair(id, bezelCode);
  }
}
