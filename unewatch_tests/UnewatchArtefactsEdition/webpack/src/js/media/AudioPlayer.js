import EventEmitter from 'wolfy87-eventemitter';
import { Howl } from 'howler';

// Wrapper for any Audio Player
class AudioPlayer {
  static DEFAULT_FADE_TIME = 1500;

  /* Constants */
  static AUTOSTART_TYPE = {
    NONE: 0,
    NORMAL: 0,
    FADE_IN: 1
  };
  static EVENT = {
    LOADED: 'onLoaded',
    PLAY: 'onPlay',
    PAUSE: 'onPause',
    STOP: 'onStop',
    SEEK: 'onSeek',
    ENDED: 'onEnded',
    POSITION_CHANGE: 'onPositionChange',
    SRC_CHANGED: 'srcChanged'
  };

  _currentSourcePath = '';
  _playing = false;
  _paused = false;
  _ended = false;
  _loaded = false;
  _actualPlayer = null;
  _volume = 1.0;
  _ee = new EventEmitter();
  _autostart = null;
  _playingInterval = null;
  _currentDefaultFadeTime = AudioPlayer.DEFAULT_FADE_TIME;

  load(filePath, autostart = false) {
    const that = this;

    this._loaded = false;
    this._autostart = autostart;

    this.stop();

    this._currentSourcePath = filePath;

    if (this._actualPlayer) {
      this.stop();
    }

    this._actualPlayer = new Howl({
      src: [filePath],
      autoplay: false,
      html5: true, //TODO: Compromise solution. Find a way to use Pizzicato with effects and short preloading time.
      onload() {
        that._onLoaded();
      },
      onend() {
        that._onEnded();
      },
      onloaderror(e){
        console.error(e);
      }
    });
  }

  setDefaultFadeTime(time) {
    this._currentDefaultFadeTime = time;
  }

  getCurrentFilePath() {
    return this._currentSourcePath;
  }

  isPlaying() {
    return this._playing;
  }

  isLoaded() {
    return this._loaded;
  }

  isPaused() {
    return this._paused;
  }

  play() {
    if (!this._playing) {
      this._playing = true;
      this._paused = false;
      this._ended = false;

      this._actualPlayer.play();

      this._ee.emitEvent(AudioPlayer.EVENT.PLAY);

      return true;
    }
    return false;
  }

  playAndListenForPlaybackPositionChange(positionPrecision) {
    this._startPlayingInteval(positionPrecision);
    return this.play();
  }

  pause() {
    if (this._playing) {
      this._paused = true;
      this._playing = false;
      this._actualPlayer.pause();
      this._ee.emitEvent(AudioPlayer.EVENT.PAUSE);
      this._stopPlayingInteval();

      return true;
    }

    return false;
  }

  stop() {
    if (this._playing || this._paused) {
      this._playing = false;
      this._actualPlayer.stop();
      this._ee.emitEvent(AudioPlayer.EVENT.STOP);
      this._stopPlayingInteval();

      return true;
    }

    return false;
  }

  seek(position) {
    if (position >= 0 && position <= this.getDuration()) {
      this._actualPlayer.seek(position);
      this._ee.emitEvent(AudioPlayer.EVENT.SEEK, this.getPosition());
      if (this._playingInterval) {
        this._onPositionChange(); // Fire onPositionChange manually because it could take some time until the interval runs next
      }
    }
  }

  getPosition() {
    return this._actualPlayer.seek();
  }

  getDuration() {
    return this._actualPlayer.duration();
  }

  setVolume(volume, fade = false, fadeDuration = this._currentDefaultFadeTime) {
    if (volume >= 0.0 && volume <= 1.0) {
      if (fade) {
        this.fadeFromCurrentTo(volume, fadeDuration);
      } else {
        this._actualPlayer.volume(volume);
      }
      this._setVolumeDontAffectPlayer(volume);
      return true;
    }

    return false;
  }

  getVolume() {
    return this._volume;
  }

  getPlayerVolume() {
    return this._actualPlayer.volume(); //Can be different from this.getVolume() when fading
  }

  fadeOut(duration = this._currentDefaultFadeTime) {
    this._actualPlayer._onfade = () => {
      this.stop();
    };
    this._actualPlayer.fade(this.getVolume(), 0, duration);
  }

  fadeIn(duration = this._currentDefaultFadeTime, useCurrentVolume = false) {
    const startVolume = useCurrentVolume ? this.getVolume() : 0;
    this._actualPlayer.volume(startVolume);
    this._actualPlayer.play();
    this._actualPlayer.fade(0, this.getVolume(), duration);
  }

  fade(
    startVolume, endVolume, duration = this._currentDefaultFadeTime) {
    this._actualPlayer.fade(startVolume,
      endVolume, duration);

    this._setVolumeDontAffectPlayer(endVolume);
  }

  fadeFromCurrentTo(endVolume, duration = this._currentDefaultFadeTime) {
    this.fade(this.getPlayerVolume(), endVolume, duration);
  }

  crossFade(otherAudioPlayer, duration = this._currentDefaultFadeTime) {
    this.fadeOut(duration);
    otherAudioPlayer.fadeIn(duration);
  }

  getActualPlayer() {
    return this._actualPlayer;
  }

  unload() {
    if (this.getActualPlayer()) {
      this.stop();
      this._currentSourcePath = '';
      //this.getActualPlayer().unload();
    }
  }

  addEventListener(event, listenerCallback) {
    this._ee.addListener(event, listenerCallback);
  }

  removeEventListener(event, listenerCallback) {
    if (listenerCallback) {
      this._ee.removeListener(event, listenerCallback);
    } else {
      this._ee.removeEvent(event);
    }
  }

  /*
   Private methods
   */

  _onLoaded() {
    this._loaded = true;
    this._ee.emitEvent(AudioPlayer.EVENT.LOADED);

    if (this._autostart === AudioPlayer.AUTOSTART_TYPE.NORMAL) {
      this.play();
    } else if (this._autostart === AudioPlayer.AUTOSTART_TYPE.FADE_IN) {
      this.fadeIn(this.DEFAULT_FADE_TIME);
    }
  }

  _onEnded() {
    this._playing = false;
    this._ended = true;

    this.seek(0); // Go back to the start automatically

    this._ee.emitEvent(AudioPlayer.EVENT.ENDED);
  }

  _startPlayingInteval(interval) {
    if (this._playingInterval) {
      this._stopPlayingInteval();
    }

    this._playingInterval = setInterval(() => {
      this._onPositionChange();
    }, interval);
  }

  _stopPlayingInteval() {
    clearInterval(this._playingInterval);
  }

  _onPositionChange() {
    // TODO: Maybe check if Position is new Position and Position is in range of the Audio Source
    this._ee.emitEvent(AudioPlayer.EVENT.POSITION_CHANGE, [this.getPosition()]);
  }

  _setVolumeDontAffectPlayer(volume) {
    this._volume = volume;
  }

}

export default AudioPlayer;
