import TypeChecker from './TypeChecker';
import ValueTypes from './TypeSafeValueTypes';

export default class TypeSafeList {
  arr = [];

  // TODO: Add methods

  static checkAllArrayElements(arr, type) {
    let equalTypes = true;
    for (let i = 0; i < arr.length && equalTypes; i += 1) {
      equalTypes = equalTypes && TypeChecker.ensure(arr[i], type,
        `The type of the value at index ${i} is not equal to ${ValueTypes.getTypeName(
          type)}`);
    }
    return equalTypes;
  }
}
