import TypeChecker from './TypeChecker';
import ValueTypes from './TypeSafeValueTypes';

export default class MultipleTypesMap {
  _nativeDict = {};

  constructor(startDict) {
    if (startDict !== undefined) {
      TypeChecker.ensure(startDict, Object);
      this._nativeDict = startDict;
    }
  }

  set(key, value, type) {
    TypeChecker.ensure(key, String, 'The key must be of type \'String\'.');
    if (type) {
      TypeChecker.ensure(value, type,
        `The type of the value '${value.toString()}' is not equal to the expected type '${ValueTypes.getTypeName(
          type)}'.`);
    }

    this._nativeDict[key] = value;
  }

  hasValue(key) {
    return !!this._nativeDict[key];
  }

  get(key, type) {
    const value = this._nativeDict[key];
    if (!type) {
      return value;
    }

    return TypeChecker.ensure(value, type) ? value : null;
  }

  isType(key, type) {
    return this.hasValue(key) && TypeChecker.check(this.get(key), type);
  }

  remove(key) {
    delete this._nativeDict[key];
  }

  getOrSomething(key, type, defaultValue) {
    TypeChecker.ensure(defaultValue, type);
    const value = this.get(key, type);
    return this.isType(key, type) && value !== null ? value : defaultValue;
  }

  getRawDict() {
    return this._nativeDict;
  }
}
