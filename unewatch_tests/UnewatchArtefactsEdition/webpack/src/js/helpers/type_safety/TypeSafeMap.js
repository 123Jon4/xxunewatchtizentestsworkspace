import ValueTypes from './TypeSafeValueTypes';
import TypeChecker from './TypeChecker';

export default class TypeSafeMap {
  _keyType = String;
  _valueType = String;
  dict = {};

  constructor(valueType, startDict, keyType = String) {
    if (!ValueTypes.isType(valueType) || !ValueTypes.isType(keyType)) {
      throw Error('These are no types');
    }
    if (startDict) {
      // TODO: Check all entries
    }
  }

  _checkType(value) {

  }

  _checkAllElements() {

  }

  static checkAllMapElements(dict, valueType, keyType = String) {
    let equalTypes = true;
    const entries = Object.entries(dict);
    for (let i = 0; i < entries.length && equalTypes; i += 1) {
      const key = entries[i][0];
      const value = entries[i][1];
      equalTypes = equalTypes && (TypeChecker.ensure(key, keyType,
        `The type of the key '${key}' is unequal to '${ValueTypes.getTypeName(
          keyType)}'`) &&
        TypeChecker.ensure(value, valueType,
          `The type '${ValueTypes.getTypeName(
            value)}' of the value at key '${key}' is unequal to '${ValueTypes.getTypeName(
            valueType)}'`));
    }
    return equalTypes;
  }
}
