import Null from './Null';
import Class from './Class';

export default class TypeSafeValueTypes {
  static HIGHER_TYPES = {
    PRIMITIVE: Symbol('primitive'),
    CLASS: Symbol('class'),
    CLASS_INSTANCE: Symbol('classInstance')
  };

  static determineType(value) {
    return TypeSafeValueTypes._getClassRepresentation(value);
  }

  static _getClassRepresentation(value) {
    switch (TypeSafeValueTypes.determineHigherType(value)) {
      default:
      case TypeSafeValueTypes.HIGHER_TYPES.PRIMITIVE:
        switch (typeof value) {
          case 'string':
            return String;
          case 'number':
            return Number;
          case 'boolean':
            return Boolean;
          case 'undefined':
            throw new Error('The value is undefined.');
          case 'function':
            return Function;
          case 'symbol':
            return Symbol;
          default:
          case 'object':
            // Null
            // Array
            // Object
            if (value === null) {
              return Null;
            } else if (value instanceof Array) {
              return Array;
            }
            return Object;
        }
      case TypeSafeValueTypes.HIGHER_TYPES.CLASS:
        return Class; // value;
      case TypeSafeValueTypes.HIGHER_TYPES.CLASS_INSTANCE:
        return value.constructor;
    }
  }

  static compareTypeOfPrimitives(value, type) {
    switch (typeof value) {
      case 'string':
        return type === String;
      case 'number':
        return type === Number;
      case 'boolean':
        return type === Boolean;
      case 'symbol':
        return type === Symbol;
      default:
        return false;
    }
  }

  static determineHigherType(value) {
    const primitiveTypeName = typeof value;
    if (primitiveTypeName === 'function') {
      if (value.name && value.name !== 'anonymous') {
        return TypeSafeValueTypes.HIGHER_TYPES.CLASS;
      }
    }

    if (primitiveTypeName === 'object') {
      if (value.constructor) {
        return TypeSafeValueTypes.HIGHER_TYPES.CLASS_INSTANCE;
      }
    }
    return TypeSafeValueTypes.HIGHER_TYPES.PRIMITIVE;
  }

  static isType(type) {
    return TypeSafeValueTypes.determineHigherType(type) ===
      TypeSafeValueTypes.HIGHER_TYPES.CLASS;
  }

  static getTypeName(value) {
    // value.constructor.name when typeof = 'function' otherwise typeof(value)
    switch (TypeSafeValueTypes.determineHigherType(value)) {
      default:
      case TypeSafeValueTypes.HIGHER_TYPES.PRIMITIVE:
        return `Primitive:${typeof value}`;
      case TypeSafeValueTypes.HIGHER_TYPES.CLASS:
        return `Class:${value.name}`;
      case TypeSafeValueTypes.HIGHER_TYPES.CLASS_INSTANCE:
        return `Instance:${value.constructor.name || 'anonymous'}`;
    }
  }
}
