// TODO: Remove this class from all usages and remove the class completely
export default class TypeSafety {
  // TODO: Add type check to some other critical code locations
  static brutalTypeChecking(object, objectLiteral) {
    if (!(object instanceof objectLiteral)) {
      throw new TypeError(
        `You tried to pass an Object of Type '${object.constructor.name
          }'. Unfortunately it doesnt match the desired Type of '${
          objectLiteral.constructor.name}'`);
    }

    return true;
  }

  static brutalTypeCheckingString(object, type) {
    if (typeof object !== type) { // eslint-disable-line valid-typeof
      let constructorName = '';
      try {
        constructorName = object.constructor.name;
      } catch (err) {
        //
      }
      throw new TypeError(
        `You tried to pass an Object of Type '${typeof object
          }${constructorName !== '' ? `(${constructorName
          })` : ''}'. Unfortunately it doesnt match the desired Type of '${
          type}'`);
    }
  }
}

