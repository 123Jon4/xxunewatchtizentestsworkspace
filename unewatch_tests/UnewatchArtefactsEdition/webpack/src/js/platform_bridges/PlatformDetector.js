export default class PlatformDetector {
  static PLATFORMS = {
    TIZEN: Symbol('tizen'),
    TIZEN_WEARABLE: Symbol('tizen_wearbale'),
    BROWSER: Symbol('browser')
  };

  static detect() {
    if (window.tizen) {
      try {
        if (window.tau.support.shape.circle) {
          return PlatformDetector.PLATFORMS.TIZEN_WEARABLE;
        }
      } catch (e) {
        //
      }
      return PlatformDetector.PLATFORMS.TIZEN;
    }

    return PlatformDetector.PLATFORMS.BROWSER;
  }
}
