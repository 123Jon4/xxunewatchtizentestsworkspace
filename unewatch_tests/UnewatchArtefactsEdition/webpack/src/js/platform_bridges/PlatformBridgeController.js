import EventEmitter from 'wolfy87-eventemitter';
import NotYetImplementedError from '../errors/NotYetImplementedError';

export default class PlatformBridgeController {
  static EVENT = {
    ON_BEZEL: Symbol('OnBezel'),
    ON_BEZEL_STOPPED: Symbol('OnBezelStopped'),
    ON_BATTERY_CHANGE: Symbol('OnBatteryChange'),
    ON_HEADSET_BATTERY_CHANGE: Symbol('OnHeadsetBatteryChange'),
    ON_HEADSET_CONNECTED: Symbol('OnHeadsetConnected'),
    ON_HEADSET_DISCONNECTED: Symbol('OnHeadsetDisconnected'),
    ON_HEADSET_CALL_BUTTON_PRESSED: Symbol('OnHeadsetCallButtonPressed'),
    ON_HEADSET_VOLUME_CHANGE: Symbol('OnHeadsetVolumeChange'),
    ON_NFC_TAG: Symbol('OnNfcTag') // Not implemented
  };

  static SCREEN_STATE = {
    AUTO: 'auto',
    OFF: 'off',
    ON: 'on'
  };

  _ee = new EventEmitter();

  enableKioskMode() {
    throw new NotYetImplementedError();
  }

  disableKioskMode() {
    throw new NotYetImplementedError();
  }

  _setKioskMode() {
    throw new NotYetImplementedError();
  }

  getKioskModeState() {
    throw new NotYetImplementedError();
  }

  setSystemVolume() {
    throw new NotYetImplementedError();
  }

  getSystemVolume() {
    throw new NotYetImplementedError();
  }

  getSystemVersion() {
    throw new NotYetImplementedError();
  }

  getSystemFreeMemory() {
    throw new NotYetImplementedError();
  }

  resolvePath() {
    throw new NotYetImplementedError();
  }

  setScreenState() {
    throw new NotYetImplementedError();
  }

  getScreenState() {
    throw new NotYetImplementedError();
  }
}
