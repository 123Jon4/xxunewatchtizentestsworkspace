import { LANGUAGE_CODE } from './Constants';

// In this project the AppController is used for anything as a Container
export default class AppController {
  constructor() {
    this._languageCode = LANGUAGE_CODE.DE;
    this._stationsManager = null;
  }
}
