import StoryManager from './story/v1_0/StoryManager';
import StationsManager from './stations/v1_0/StationsManager';

export default class StoryDrivenLogicController {
  storyManager = null;
  stationsManager = null;

  constructor(stationsManager, storyManager) {
    this.initStationsManager(stationsManager);
    this.initStoryManager(storyManager);
  }
  // TODO: Add all important function to this class so that the story logic
  // can be controlled easily and it will be managed from one point as well as events

  initStoryManager(story) {
    this.storyManager = new StoryManager(story);
  }

  initStationsManager(stations) {
    this.stationsManager = new StationsManager(stations);
  }

  getStoryManager() {
    return this.storyManager;
  }

  getStationsManager() {
    return this.stationsManager;
  }

  reset() {
    this.storyManager.reset();
    this.stationsManager.reset();
  }
}
