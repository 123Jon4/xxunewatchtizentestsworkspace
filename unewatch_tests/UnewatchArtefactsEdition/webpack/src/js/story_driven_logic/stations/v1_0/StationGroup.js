import Station from './Station';

export default class StationGroup {
  id = 0;
  name = '';
  comment = '';
  data = {};
  locales = [];
  subStations = {};

  static createFromJSON(json) {
    const group = new StationGroup();
    group.id = json.id;
    group.name = json.name;
    group.comment = json.comment;
    group.data = json.data;
    group.locales = json.locales;

    group.subStations = {};
    json.subStations.forEach(
      (curStation) => {
        const station = Station.createFromJSON(curStation);
        station.group = group;
        group.subStations[station.id] = station;
      });
    return group;
  }
}
