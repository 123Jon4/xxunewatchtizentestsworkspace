import JSONRepresentingObject from '../../../JSONRepresentingObject';

export default class Station extends JSONRepresentingObject {
  static COMPOSITE_KEY_SEPARATOR = '.';
  id = 0;
  name = '';
  comment = '';
  data = { visited: false };
  locales = [];
  commands = {};
  // keyframes;

  group;

  getVisited() {
    return this.data.visited;
  }

  setVisited(b) {
    this.data.visited = b;
  }

  getCompositeKey() {
    return `${this.group.id}${Station.COMPOSITE_KEY_SEPARATOR}${this.id}`;
  }

  static createFromJSON(json) {
    const station = new Station();
    station.id = json.id;
    station.name = json.name;
    station.comment = json.comment;
    station.data = json.data;
    station.commands = json.commands;
    station.locales = json.locales;
    return station;
  }
}

export class DataFields {

}
