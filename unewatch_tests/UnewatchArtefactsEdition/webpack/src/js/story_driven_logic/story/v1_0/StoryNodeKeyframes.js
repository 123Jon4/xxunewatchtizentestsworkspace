import JSONRepresentingObject from '../../../JSONRepresentingObject';

export default class StoryNodeKeyframes extends JSONRepresentingObject {
  nodeId = 0;
  general = [];
  localeSpecific = {};

  hasLocaleSpecificKeyframes(languageCode) {
    const keyframes = this.localeSpecific[languageCode];
    return !!keyframes && keyframes.length > 0;
  }

  getLocaleSpecificKeyframes(languageCode) {
    return this.localeSpecific[languageCode] || [];
  }

  hasLocaleSpecificKeyframeAtTime(languageCode, time) {
    return !!this.getLocaleSpecificKeyframeAtTime(languageCode, time);
  }

  getLocaleSpecificKeyframeAtTime(languageCode, time) {
    const localeKeyframes = this.getLocaleSpecificKeyframes(languageCode);
    if (!localeKeyframes) {
      return null;
    }
    const matchingKeyframes = localeKeyframes.filter(
      lKey => lKey.at === time);

    return matchingKeyframes.length > 0 ? matchingKeyframes[0] : null;
  }

  static createFromJSON(json) {
    const keyframes = new StoryNodeKeyframes();
    keyframes.nodeId = json.nodeId;
    keyframes.general = (json.general || []).map(
      curKeyframe => StoryNodeKeyframe.createFromJSON(curKeyframe));
    keyframes.localeSpecific = {};
    Object.entries(json.localeSpecific ||
      {}).forEach((curLocaleKeyframesPair) => {
      const localeKey = curLocaleKeyframesPair[0];
      const localeKeyframes = curLocaleKeyframesPair[1];
      const parsedKeyframes = (localeKeyframes || []).map(
        curKeyframe => StoryNodeKeyframe.createFromJSON(curKeyframe));
      keyframes.localeSpecific[localeKey] = parsedKeyframes; // eslint-disable-line no-param-reassign
    });
    return keyframes;
  }
}

export class StoryNodeKeyframe extends JSONRepresentingObject {
  at = 0;
  atSpecial = '';
  data = {};
  commands = [];
  triggered = false;
  triggeredAt = -1;

  /**
   * With the help of the time difference one could only
   * accept keyframes which are in a given time tolerance
   * @returns {number}
   */
  getTriggeredTimeDifference() {
    return this.triggered ? this.triggeredAt - this.at : 0;
  }

  reset() {
    this.triggered = false;
    this.triggeredAt = -1;
  }

  clone() {
    const c = new StoryNodeKeyframe();
    c.at = this.at;
    c.atSpecial = this.atSpecial;
    c.data = this.data || {};
    c.commands = this.commands;
    c.triggered = this.triggered;
    c.triggeredAt = this.triggeredAt;

    return c;
  }

  static createFromJSON(json) {
    const keyframe = new StoryNodeKeyframe();
    keyframe.at = json.at;
    keyframe.atSpecial = json.atSpecial || '';
    keyframe.data = json.data || {};
    keyframe.commands = json.commands || [];
    return keyframe;
  }
}
