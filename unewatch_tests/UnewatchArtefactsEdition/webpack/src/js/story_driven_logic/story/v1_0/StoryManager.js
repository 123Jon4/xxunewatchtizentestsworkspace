import { NODE_TRIGGERS, SPECIAL_KEYFRAMES } from './StoryConstants';
import TypeChecker from '../../../helpers/type_safety/TypeChecker';
import Story from './Story';
import PrimitiveKeyframesManager from '../../../../../../js/primitives/PrimitiveKeyframesManager';

export default class StoryManager {
  _story;

  _started = false;

  _currentNode;
  _keyframesManager;
  _nodeHistory = [];
  _chosenPaths = {};

  _currentNodeEnded = false;
  _stationsSinceCurrentNodeEnded = 0;

  _language = '';

  constructor(story, languageCode) {
    this.setStory(story);
    this.setLanguage(languageCode);
    this._keyframesManager = new PrimitiveKeyframesManager();
  }

  setStory(story) {
    TypeChecker.ensure(story, Story);
    this._story = story;
  }

  setLanguage(lng) {
    this._language = lng;
    if (this.hasCurrentNode()) {
      this._keyframesManager.setLanguage(lng);
      this.restartCurrentNode();
    }
  }

  start(compositeKey = '') {
    // Search for a story node with the trigger start or for the key === key
    let node = null;
    if (compositeKey !== '') {
      node = this._story.getNodeByCompositeKey(compositeKey);
    } else {
      node = this._story.getNodeWithTrigger(NODE_TRIGGERS.START);
    }
    if (!node) {
      return false;
    }
    // TODO: Emit event
    this._started = true;
    this.setCurrentNode(node);
    return true;
  }

  restartCurrentNode() {
    this._keyframesManager.resetKeyframes();
  }

  nodeEnded() {
    this._currentNodeEnded = true;
    this._currentNodeSpecifiedNextRequierements = false;
    this._keyframesManager.getSpecialKeyframe(
      SPECIAL_KEYFRAMES.END);
  }

  next() {
    // Set next node as current node if current node has next
    const nextNode = this._story.getNodeByCompositeKey(
      this._story.getFlowControlNodeByNode(this.getCurrentNode())
          .getDefaultPath().nodeId);
    this.setCurrentNode(nextNode);
    return nextNode;
  }

  reset() {
    this._nodeHistory = [];
    this._started = false;
    this.setCurrentNode(null);
    this._chosenPaths = {};
  }

  hasNext() {
    // Check if current node has next
    return this._story.hasFlowControlNode(this.getCurrentNode().group.id,
      this.getCurrentNode().id) &&
      this._story.getFlowControlNodeByNode(this.getCurrentNode())
          .hasPaths();
  }

  hasNextDefault() {
    // Check if current node has next
    return this.hasNext() &&
      this._story.getFlowControlNodeByNode(this.getCurrentNode())
          .hasDefaultPath();
  }

  choosePath(pathName) {
    this._chosenPaths[this._currentNode.getCompositeKey()] = this._currentNode.getPath(
      pathName);
  }

  defaultPathRequirementsFulfilled() {
    // TODO: Do this at the very end
    /*
     if (!this.hasCurrentNode() || !this.hasNextDefault()) {
     return false;
     }
     const defaultPath = this.getCurrentNode().getDefaultPath();
     const fulfilled = false;
     // Check if requirements are fulfilled
     switch (defaultPath.name) {
     case PATH_REQUIREMENT_NAMES.AFTER_RANDOM_STATIONS: {
     const maxStations = defaultPath.params.break;
     break;
     }
     case 'dsd':
     break;
     default:
     }

     return fulfilled;
     */
    return true;
  }

  isRunning() {
    return this._started;
  }

  hasCurrentNode() {
    return !!this.getCurrentNode();
  }

  getCurrentNode() {
    return this._currentNode;
  }

  setCurrentNode(node) {
    this.resetStationsSinceLastNodeCounter();
    this._currentNodeEnded = false;
    this._currentNodeSpecifiedNextRequierements = false;
    this._keyframesManager.init(
      this._story.getKeyframesForNode(node.group.id, node.id), this._language);

    this._currentNode = node;
  }

  incrementCounterOfStationsSinceCurrentNodeEnded() {
    this._stationsSinceCurrentNodeEnded += 1;
  }

  resetStationsSinceLastNodeCounter() {
    this._stationsSinceCurrentNodeEnded = 0;
  }

  sendTrigger(triggerName) {
    const node = this._story.getNodeWithTrigger(triggerName);
    if (!node) {
      return false;
    }
    this.setCurrentNode(node);
    return true;
  }

  _addCurrentNodeToHistory() {
    this._addToHistory(this._currentNode.id);
  }

  _addToHistory(id) {
    this._nodeHistory.push(id);
  }

  getNodeHistory() {
    return this._nodeHistory;
  }

  getChosenPaths() {
    return this._chosenPaths;
  }

  hasChosenPathFor(compositeKey) {
    return this.getChosenPath(compositeKey);
  }

  getChosenPath(compositeKey) {
    return this._chosenPaths[compositeKey];
  }

  setChosenPathOfCurrentNode(pathName) {
    if (!this._story.getFlowControlNodeByNode(
        this._story.getNodeByCompositeKey(this._currentNode.getCompositeKey()))
             .hasPath(pathName)) {
      throw new Error(
        'The  path name doesn\'t exist in the current FlowControlNode');
    }

    this._chosenPaths[this._currentNode.getCompositeKey()] = pathName;
  }

  getKeyframesManager() {
    return this._keyframesManager;
  }
}
