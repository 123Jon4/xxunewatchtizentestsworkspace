import JSONRepresentingObject from '../../../JSONRepresentingObject';
import StoryNode from './StoryNode';

export default class StoryNodeGroup extends JSONRepresentingObject {
  id = 0;
  comment = '';
  subNodes = {};
  locales = [];

  static createFromJSON(json) {
    const group = new StoryNodeGroup();
    group.id = json.id;
    group.comment = json.comment;
    group.locales = json.locales;
    group.subNodes = {};
    json.subNodes.forEach((curNode) => {
        const node = StoryNode.createFromJSON(curNode);
        node.group = group;
        group.subNodes[node.id] = node;
      }
    );
    return group;
  }
}
