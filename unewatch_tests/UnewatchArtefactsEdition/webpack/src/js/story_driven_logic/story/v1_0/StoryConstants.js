export const NODE_TRIGGERS = {
  START: 'start',
  END_WITH_ALL_STATIONS: 'endWithAllStations',
  END_WITH_SOME_STATIONS: 'endWithSomeStations'
};

export const PATH_REQUIREMENT_NAMES = {
  AFTER_STATIONS: Symbol('afterStations'),
  AFTER_RANDOM_STATIONS: Symbol('afterRandomStations'),
  WAIT_FOR_USER_INPUT: Symbol('waitForUserInput'),
  IMMEDIATELY: Symbol('immediately')
};

export const SPECIAL_KEYFRAMES = {
  END: 'end'
};

export const VIEWS = {
  UEI: 'uei',
  HYPERPARKS_OS: 'hyperparksOS',
  HOPE: 'hope',
  FINAL_DECISION: 'finalDecision',
  STANDBY: 'standby',
  OLDSCHOOL: 'oldschool',
  SETUP: 'setup',
  ADMIN_MENU: 'adminMenu'
};
const STORY_CONSTANTS = { NODE_TRIGGERS, SPECIAL_KEYFRAMES, VIEWS };
export default STORY_CONSTANTS;
