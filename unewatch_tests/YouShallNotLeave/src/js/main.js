var screen;
var background;
var text;
var canLeave = true;

var freeText = 'You may leave now or stay forever.';
var blockedText = 'You shall not leave!';
var textChangeTimeout;

function main() {
	screen = document.body;
	background = document.getElementById('background');
	text = document.getElementById('text');
	screen.addEventListener('click', function() {
		canLeave=!canLeave;
		setLeavable(canLeave);
	});

	setLeavable(canLeave);
}

function setLeavable(canLeave){
	var newText = '';
	if (canLeave) {
		background.classList.remove('blocked');
		newText = freeText;
	} else {
		background.classList.add('blocked');
		newText = blockedText;
	}

	if (textChangeTimeout) {
		clearTimeout(textChangeTimeout);
	}
	textChangeTimeout = setTimeout(function() {
		text.textContent = newText;
		if(canLeave){
			document.removeEventListener("visibilitychange", visibilityChange);
		}else{
			document.addEventListener("visibilitychange", visibilityChange);
		}
	}, 150);
}

function visibilityChange() {
	if (document.hidden || document.webkitHidden) {
		var id = tizen.application.getCurrentApplication().appInfo.id;
		// launch directly
		tizen.application.launch(id);
		console.log(tizen.application.getCurrentApplication().appInfo.id);
	}
}

main();