'use strict';

Vue.component("wormhole", {
  template: '<div class="">\n<div class="center both wormhole-text " :class="{show:showText}">{{text}}</div>\n              <div class="wormhole wormhole-shadow" @click="animate" ref="wormhole"></div>\n</div>\n            ',
  data: function data() {
    return {
      anim: null,
      text: 'Marxenhaus',
      showText: false
    };
  },

  methods: {
    animate: function animate() {
      var that = this;
      if (this.anim) {
        this.anim.pause();
      }
      anime({
        targets: this.$refs.wormhole,
        scale: [0, 1.0],
        easing: 'easeInSine',
        duration: anime.random(10000, 10000),
        begin: function begin() {
          that.showText = false;
        },
        complete: function complete() {
          that.showText = true;
          // TODO: Wait 5s then show some console text (Saving..., Comrpessing... Taking Snapshot of the Artefact...), Add sonar effects from outside to the middle
        }
      });
    }
  },
  mounted: function mounted() {
    this.animate();
  }
});

var app = new Vue({
  el: "#app",
  data: {},
  methods: {},
  mounted: function mounted() {}
});