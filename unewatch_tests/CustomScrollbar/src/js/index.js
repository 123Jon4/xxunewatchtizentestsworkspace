"use strict";

Vue
		.component(
				"default-content",
				{
					template : "\n              <p class=\"template text-center\">\n                Android Wear<br>\n                Tizen (Samsung Gear)<br>\n                <span class=\"unsupported\">Apple Watch</span><br>\n                .<br>\n                .<br>          \n                .<br>\n                .<br>\n                .<br>\n     Scroll if you dare!<br>\n                \\ /<br>\n                .<br>\n                .<br>\n                .<br>\n                .<br>\n                .<br>\n                .<br>\n                .<br>\n                .<br>\n                .<br>\n                .<br>\n  Hello!<br>\n                .<br>\n              </p>\n            "
				});

Vue
		.component(
				"circular-scroller",
				{
					template : "\n<div class=\"hide\" style=\"position:absolute;height:100%;width:100%;\n    box-sizing: border-box;pointer-events:none;\">\n  <svg style=\"overflow: visible;\" >\n    <path id=\"loader-trail\" fill=\"none\" :stroke-width=\"strokeWidth\" stroke=\"#E8F6FD\" />\n    <path id=\"loader\" fill=\"none\" :stroke-width=\"strokeWidth\" stroke=\"#00ACEE\" />\n  </svg>\n</div>\n",
					data : function data() {
						return {
							padding : 20,
							strokeWidth : 10,
							bar : null,
							trail : null,
							scrollBarLength : 0,
							value : 0
						};
					},

					methods : {
						setProgress : function setProgress(v) {
							console.log("Progress:", v);
							this.bar.set(v);
							this.value = v;
						},
						setScrollbarLength : function setScrollbarLength(v) {
							this.scrollBarLength = v;
						},
						repaint : function repaint() {
							console.log("V", this.bar.value());
							this.bar.set(this.value);
						},
						show : function show(b) {
							if (b) {
								this.$el.classList.add("show");
								this.$el.classList.remove("hide");
							} else {
								this.$el.classList.add("hide");
								this.$el.classList.remove("show");
							}
						}
					},
					mounted : function mounted() {
						// progressbar.js@1.0.0 version is used
						// Docs: http://progressbarjs.readthedocs.org/en/1.0.0/
						var width = this.$el.clientWidth;
						console.log(width);
						var arc = describeArc(width / 2, width / 2, width / 2
								- this.padding / 2 - this.strokeWidth / 2, 45,
								135);
						loader.setAttribute("d", arc);
						document.getElementById("loader-trail").setAttribute(
								"d", arc);
						var that = this;
						var bar = new ProgressBar.Path("#loader", {
							easing : "bounce",
							duration : 1400,
							step : function step(state, bar, attachment) {
								var path = bar.path;
								var pathLength = path.getTotalLength();
								var curValue = bar.value();
								var scrollbarLength = pathLength
										* that.scrollBarLength;
								var offset = path.getTotalLength()
										* (-1 + -curValue) - scrollbarLength
										* (-1 + -curValue);
								path.style.strokeDashoffset = offset;
								path.style.strokeDasharray = scrollbarLength
										+ "," + pathLength;
							}
						});

						var trail = new ProgressBar.Path("#loader-trail", {});

						trail.path.setAttribute("stroke-linecap", "round");
						trail.set(1);

						bar.path.setAttribute("stroke-linecap", "round");
						bar.set(0);

						// Source:
						// https://stackoverflow.com/questions/5736398/how-to-calculate-the-svg-path-for-an-arc-of-a-circle
						function polarToCartesian(centerX, centerY, radius,
								angleInDegrees) {
							var angleInRadians = (angleInDegrees - 90)
									* Math.PI / 180.0;

							return {
								x : centerX + radius * Math.cos(angleInRadians),
								y : centerY + radius * Math.sin(angleInRadians)
							};
						}

						function describeArc(x, y, radius, startAngle, endAngle) {
							var start = polarToCartesian(x, y, radius, endAngle);
							var end = polarToCartesian(x, y, radius, startAngle);

							var largeArcFlag = endAngle - startAngle <= 180 ? "0"
									: "1";

							var d = [ "M", start.x, start.y, "A", radius,
									radius, 0, largeArcFlag, 0, end.x, end.y ]
									.join(" ");

							return d;
						}
						this.bar = bar;
						this.trail = trail;
					}
				});

var app = new Vue(
		{
			el : "#app",
			data : {},
			methods : {
				updateBar : function updateBar() {
					var scrollElement = document
							.getElementById("scroll-parent");
					var contentElement = scrollElement.firstChild;
					var d = contentElement.clientHeight, c = scrollElement.clientHeight;
					var ratio = c / d;
					console.log("Ratio", ratio);
					this.$refs.scroller.setScrollbarLength(ratio);
					this.$refs.scroller.repaint();
				}
			},
			mounted : function mounted() {
				var _this = this;

				var scrollElement = document.getElementById("scroll-parent");
				var contentElement = scrollElement.firstChild;
				scrollElement
						.addEventListener(
								"scroll",
								function(evt) {
									// Source:
									// https://stackoverflow.com/a/19700208
									var s = scrollElement.scrollTop, d = contentElement.clientHeight, c = scrollElement.clientHeight, scrollPercent = s
											/ (d - c) * 100;
									_this.$refs.scroller
											.setProgress(-scrollPercent / 100);
								});
				scrollElement.addEventListener("touchstart", function() {
					_this.updateBar();
					_this.$refs.scroller.show(true);
				});
				scrollElement.addEventListener("touchend", function() {
					_this.$refs.scroller.show(false);
				});
				this.updateBar();
			}
		});